const sveltePreprocess = require('svelte-preprocess');

let configJs = {};

let isSvelteLanguageServer = false;
for (let argv of process.argv) {
  if (argv.indexOf('svelte-language-server') !== -1) {
    isSvelteLanguageServer = true;
    break
  }
}

if (!isSvelteLanguageServer) {
  configJs = {
    preprocess: sveltePreprocess({
      scss: {
        includePaths: ['src'],
      },
      postcss: {
        plugins: [require('autoprefixer')],
      },
    }),
  };
}

module.exports = configJs;
