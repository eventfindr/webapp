const eventsOrderBy = [
  { name: "Date (ascendant)", value: "DATE_ASC" },
  { name: "Date (descendant)", value: "DATE_DESC" },
];

export { eventsOrderBy };
