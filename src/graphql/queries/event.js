import { gql } from "apollo-boost";

const GET_EVENTS = gql`
  query events(
    $page: Int
    $featuredOnly: Boolean
    $orderBy: EventOrderBy
    $providerFilter: [String!]
    $startDate: DateTimeUtc
    $endDate: DateTimeUtc
  ) {
    events(
      page: $page
      featuredOnly: $featuredOnly
      orderBy: $orderBy
      providerFilter: $providerFilter
      startDate: $startDate
      endDate: $endDate
    ) {
      id
      name
      summary
      provider
      imageUrl
      url
      date
      featured
    }
  }
`;

const GET_AUTOCOMPLETE_EVENTS = gql`
  query autocompleteEvents($query: String!) {
    autocompleteEvents(query: $query) {
      id
      name
    }
  }
`;

const GET_EVENT_BY_ID = gql`
  query eventById($id: String!) {
    eventById(id: $id) {
      id
      name
      summary
      provider
      imageUrl
      url
      date
      featured
    }
  }
`;

const SEARCH_EVENTS_BY_GEOLOCATION = gql`
  query searchEventsByGeolocation($latitude: Float!, $longitude: Float!) {
    searchEventsByGeolocation(latitude: $latitude, longitude: $longitude) {
      id
      name
      summary
      provider
      imageUrl
      url
      date
      featured
    }
  }
`;

export {
  GET_EVENTS,
  GET_AUTOCOMPLETE_EVENTS,
  GET_EVENT_BY_ID,
  SEARCH_EVENTS_BY_GEOLOCATION,
};
