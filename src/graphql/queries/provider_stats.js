import { gql } from "apollo-boost";

const GET_PROVIDERS_STATS = gql`
  query {
    providersStats {
      provider
      eventsNumber
    }
  }
`;

const GET_PROVIDERS = gql`
  query {
    providers: providersStats(withoutTotal: true) {
      name: provider
    }
  }
`;

export { GET_PROVIDERS_STATS, GET_PROVIDERS };
