import anime from "animejs/lib/anime.es.js";

const slide = (targets, duration, from, to) => {
  targets.style.transform = `translateX(${from}%)`;

  const translateX = `${to}%`;
  const anim = anime.timeline({
    easing: "easeInOutSine",
    duration,
  });

  anim.add({
    targets,
    translateX,
  });

  return anim.finished;
};

export default slide;
