import { sineOut } from "svelte/easing";

const defaultDuration = 300;
const defaultDelay = defaultDuration;

export const fadeIn = (
  node,
  { delay = defaultDelay, duration = defaultDuration }
) => ({
  duration,
  delay,
  easing: sineOut,
  css: (t) => `opacity: ${t}`,
});

export const fadeOut = (node, { delay = 0, duration = defaultDuration }) => ({
  duration,
  delay,
  easing: sineOut,
  css: (t) => `opacity: ${t}`,
});
