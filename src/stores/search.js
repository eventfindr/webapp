import { writable } from "svelte/store";

const featuredOnly = writable(true);
const orderBy = writable("DATE_ASC");
const providers = writable([]);
const startDate = writable(null);
const endDate = writable(null);

export { featuredOnly, orderBy, providers, startDate, endDate };
